package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data

public class LoginPageLocators {

    private By usernameLocator = By.id("username");

    private By passwordLocator = By.xpath(".//input[contains(@placeholder,'Password')]");
    //private By passwordLocator = By.id("password");

    private By loginButtonLocator = By.id("enter");
}
