package com.safebear.auto.tests;

import com.safebear.auto.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class LoginTests extends BaseTest {
    @Test
    public void loginTest() {



    List<List<String>> rows = Utils.getCsvData("src/test/resources/users.csv");
    //Step 1 ACTION: open our web application in the browser
        for(List<String> row:rows){

        driver.get(Utils.getURL());

        Assert.assertEquals(loginPage.getPageTitle(), "Login Page", "CSV feed - The Login Page did not open, or the title text has changed");

        //Step 2 ACTION: enter Username and Password
        loginPage.enterUsername(row.get(0));
        loginPage.enterPassword(row.get(1));

        //Step 3 ACTION: press the login button
        loginPage.clickLoginButton();
        //Step 3 EXPECTED RESULT: check that we are now on the Tools page

        Assert.assertEquals(toolsPage.getPageTitle(), "Tools Page", "CSV feed - The login page didn't open, or the title text has changed");
//original
        // public void loginTest(){
        //Step 1 ACTION: open our web application in the browser
        // driver.get(Utils.getURL());
        //Assert.assertEquals(loginPage.getPageTitle(),"Login Page","Custom msg - The Login Page did not open, or the title text has changed");
        //Step 2 ACTION: enter Username and Password
        //loginPage.enterUsername("tester");
        //loginPage.enterPassword("letmein");
        //Step 3 ACTION: press the login button
        //loginPage.clickLoginButton();
        //Step 3 EXPECTED RESULT: check that we are now on the Tools page
        //Assert.assertEquals(toolsPage.getPageTitle(),"Tools Page", "The login page didn't open, or the title

    }
}



}
